/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define REL_00_Pin GPIO_PIN_0
#define REL_00_GPIO_Port GPIOC
#define REL_01_Pin GPIO_PIN_1
#define REL_01_GPIO_Port GPIOC
#define REL_21_Pin GPIO_PIN_2
#define REL_21_GPIO_Port GPIOC
#define REL_02_Pin GPIO_PIN_3
#define REL_02_GPIO_Port GPIOC
#define REL_27_Pin GPIO_PIN_0
#define REL_27_GPIO_Port GPIOA
#define REL_03_Pin GPIO_PIN_1
#define REL_03_GPIO_Port GPIOA
#define REL_26_Pin GPIO_PIN_2
#define REL_26_GPIO_Port GPIOA
#define REL_04_Pin GPIO_PIN_3
#define REL_04_GPIO_Port GPIOA
#define REL_25_Pin GPIO_PIN_4
#define REL_25_GPIO_Port GPIOA
#define REL_05_Pin GPIO_PIN_5
#define REL_05_GPIO_Port GPIOA
#define REL_24_Pin GPIO_PIN_6
#define REL_24_GPIO_Port GPIOA
#define REL_06_Pin GPIO_PIN_7
#define REL_06_GPIO_Port GPIOA
#define REL_23_Pin GPIO_PIN_4
#define REL_23_GPIO_Port GPIOC
#define REL_07_Pin GPIO_PIN_5
#define REL_07_GPIO_Port GPIOC
#define REL_22_Pin GPIO_PIN_0
#define REL_22_GPIO_Port GPIOB
#define REL_08_Pin GPIO_PIN_1
#define REL_08_GPIO_Port GPIOB
#define REL_09_Pin GPIO_PIN_7
#define REL_09_GPIO_Port GPIOE
#define REL_10_Pin GPIO_PIN_9
#define REL_10_GPIO_Port GPIOE
#define REL_11_Pin GPIO_PIN_11
#define REL_11_GPIO_Port GPIOE
#define REL_12_Pin GPIO_PIN_13
#define REL_12_GPIO_Port GPIOE
#define REL_13_Pin GPIO_PIN_15
#define REL_13_GPIO_Port GPIOE
#define REL_14_Pin GPIO_PIN_11
#define REL_14_GPIO_Port GPIOB
#define REL_15_Pin GPIO_PIN_13
#define REL_15_GPIO_Port GPIOB
#define REL_16_Pin GPIO_PIN_15
#define REL_16_GPIO_Port GPIOB
#define REL_17_Pin GPIO_PIN_9
#define REL_17_GPIO_Port GPIOD
#define REL_18_Pin GPIO_PIN_11
#define REL_18_GPIO_Port GPIOD
#define LED_2_Pin GPIO_PIN_12
#define LED_2_GPIO_Port GPIOD
#define REL_19_Pin GPIO_PIN_13
#define REL_19_GPIO_Port GPIOD
#define LED_1_Pin GPIO_PIN_14
#define LED_1_GPIO_Port GPIOD
#define REL_20_Pin GPIO_PIN_15
#define REL_20_GPIO_Port GPIOD
#define USART_RE_DE_Pin GPIO_PIN_8
#define USART_RE_DE_GPIO_Port GPIOA

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
