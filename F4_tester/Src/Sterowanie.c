/*

 * Sterowanie.c
 *
 *  Created on: 17 maj 2017
 *      Author: Jakub
 */


#include "Sterowanie.h"


//nucleo

void Sterowanie_przekaznikami(uint8_t *tablica,uint16_t size)
{

//discovery

//pierwsza grupa od lewej (1-8)
if(tablica[1] & 1) HAL_GPIO_WritePin(REL_01_GPIO_Port,REL_01_Pin,SET);//REL_1
else HAL_GPIO_WritePin(REL_01_GPIO_Port,REL_01_Pin,RESET);

if(tablica[1] & 1<<1) HAL_GPIO_WritePin(REL_02_GPIO_Port,REL_02_Pin,SET);//REL_2
else HAL_GPIO_WritePin(REL_02_GPIO_Port,REL_02_Pin,RESET);

if(tablica[1] & 1<<2) HAL_GPIO_WritePin(REL_03_GPIO_Port,REL_03_Pin,SET);//REL_3
else HAL_GPIO_WritePin(REL_03_GPIO_Port,REL_03_Pin,RESET);

if(tablica[1] & 1<<3) HAL_GPIO_WritePin(REL_04_GPIO_Port,REL_04_Pin,SET);//REL_4
else HAL_GPIO_WritePin(REL_04_GPIO_Port,REL_04_Pin,RESET);

if(tablica[1] & 1<<4) HAL_GPIO_WritePin(REL_05_GPIO_Port,REL_05_Pin,SET);//REL_5
else HAL_GPIO_WritePin(REL_05_GPIO_Port,REL_05_Pin,RESET);

if(tablica[1] & 1<<5) HAL_GPIO_WritePin(REL_06_GPIO_Port,REL_06_Pin,SET);//REL_6
else HAL_GPIO_WritePin(REL_06_GPIO_Port,REL_06_Pin,RESET);

if(tablica[1] & 1<<6) HAL_GPIO_WritePin(REL_07_GPIO_Port,REL_07_Pin,SET);//REL_7
else HAL_GPIO_WritePin(REL_07_GPIO_Port,REL_07_Pin,RESET);

if(tablica[1] & 1<<7) HAL_GPIO_WritePin(REL_08_GPIO_Port,REL_08_Pin,SET);//REL_8
else HAL_GPIO_WritePin(REL_08_GPIO_Port,REL_08_Pin,RESET);
//koniec pierwsza grupa od lewej

//druga grupa od lewej (9-16)
if(tablica[2] & 1) HAL_GPIO_WritePin(REL_09_GPIO_Port,REL_09_Pin,SET);//REL_9
else HAL_GPIO_WritePin(REL_09_GPIO_Port,REL_09_Pin,RESET);

if(tablica[2] & 1<<1) HAL_GPIO_WritePin(REL_10_GPIO_Port,REL_10_Pin,SET);//REL_10
else HAL_GPIO_WritePin(REL_10_GPIO_Port,REL_10_Pin,RESET);

if(tablica[2] & 1<<2) HAL_GPIO_WritePin(REL_11_GPIO_Port,REL_11_Pin,SET);//REL_11
else HAL_GPIO_WritePin(REL_11_GPIO_Port,REL_11_Pin,RESET);

if(tablica[2] & 1<<3) HAL_GPIO_WritePin(REL_12_GPIO_Port,REL_12_Pin,SET);//REL_12
else HAL_GPIO_WritePin(REL_12_GPIO_Port,REL_12_Pin,RESET);

if(tablica[2] & 1<<4) HAL_GPIO_WritePin(REL_13_GPIO_Port,REL_13_Pin,SET);//REL_13
else HAL_GPIO_WritePin(REL_13_GPIO_Port,REL_13_Pin,RESET);

if(tablica[2] & 1<<5) HAL_GPIO_WritePin(REL_14_GPIO_Port,REL_14_Pin,SET);//REL_14
else HAL_GPIO_WritePin(REL_14_GPIO_Port,REL_14_Pin,RESET);

if(tablica[2] & 1<<6) HAL_GPIO_WritePin(REL_15_GPIO_Port,REL_15_Pin,SET);//REL_15
else HAL_GPIO_WritePin(REL_15_GPIO_Port,REL_15_Pin,RESET);

if(tablica[2] & 1<<7) HAL_GPIO_WritePin(REL_16_GPIO_Port,REL_16_Pin,SET);//REL_16
else HAL_GPIO_WritePin(REL_16_GPIO_Port,REL_16_Pin,RESET);
//koniec druga grupa od lewej

//trzecia grupa od lewej (16-24)
if(tablica[3] & 1) HAL_GPIO_WritePin(REL_17_GPIO_Port,REL_17_Pin,SET);//REL_17
else HAL_GPIO_WritePin(REL_17_GPIO_Port,REL_17_Pin,RESET);

if(tablica[3] & 1<<1) HAL_GPIO_WritePin(REL_18_GPIO_Port,REL_18_Pin,SET);//REL_18
else HAL_GPIO_WritePin(REL_18_GPIO_Port,REL_18_Pin,RESET);

if(tablica[3] & 1<<2) HAL_GPIO_WritePin(REL_19_GPIO_Port,REL_19_Pin,SET);//REL_19
else HAL_GPIO_WritePin(REL_19_GPIO_Port,REL_19_Pin,RESET);

if(tablica[3] & 1<<3) HAL_GPIO_WritePin(REL_20_GPIO_Port,REL_20_Pin,SET);//REL_20
else HAL_GPIO_WritePin(REL_20_GPIO_Port,REL_20_Pin,RESET);

if(tablica[3] & 1<<4) HAL_GPIO_WritePin(REL_21_GPIO_Port,REL_21_Pin,SET);//REL_21
else HAL_GPIO_WritePin(REL_21_GPIO_Port,REL_21_Pin,RESET);

if(tablica[3] & 1<<5) HAL_GPIO_WritePin(REL_22_GPIO_Port,REL_22_Pin,SET);//REL_22
else HAL_GPIO_WritePin(REL_22_GPIO_Port,REL_22_Pin,RESET);

if(tablica[3] & 1<<6) HAL_GPIO_WritePin(REL_23_GPIO_Port,REL_23_Pin,SET);//REL_23
else HAL_GPIO_WritePin(REL_23_GPIO_Port,REL_23_Pin,RESET);

if(tablica[3] & 1<<7) HAL_GPIO_WritePin(REL_24_GPIO_Port,REL_24_Pin,SET);//REL_24
else HAL_GPIO_WritePin(REL_24_GPIO_Port,REL_24_Pin,RESET);
//koniec czwarta grupa od lewej

//czwarta gRupa od lewej (25-27)
if(tablica[4] & 1) HAL_GPIO_WritePin(REL_25_GPIO_Port,REL_25_Pin,SET);//REL_25
else HAL_GPIO_WritePin(REL_25_GPIO_Port,REL_25_Pin,RESET);

if(tablica[4] & 1<<1) HAL_GPIO_WritePin(REL_26_GPIO_Port,REL_26_Pin,SET);//REL_26
else HAL_GPIO_WritePin(REL_26_GPIO_Port,REL_26_Pin,RESET);

if(tablica[4] & 1<<2) HAL_GPIO_WritePin(REL_27_GPIO_Port,REL_27_Pin,SET);//REL_27
else HAL_GPIO_WritePin(REL_27_GPIO_Port,REL_27_Pin,RESET);


//koniec czwarta grupa od lewej





	/*
	//pierwsza grupa od lewej (1-8)
if(tablica[1] & 1) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,RESET);

if(tablica[1] & 1<<1) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,RESET);

if(tablica[1] & 1<<2) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,RESET);

if(tablica[1] & 1<<3) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,RESET);

if(tablica[1] & 1<<4) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,RESET);

if(tablica[1] & 1<<5) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,RESET);

if(tablica[1] & 1<<6) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,RESET);

if(tablica[1] & 1<<1) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,RESET);
	//koniec pierwsza grupa od lewej

//druga grupa od lewej (9-16)
if(tablica[2] & 1) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_8,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_8,RESET);

if(tablica[2] & 1<<1) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_9,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_9,RESET);

if(tablica[2] & 1<<2) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,RESET);

if(tablica[2] & 1<<3) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_11,SET);
else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_11,RESET);

if(tablica[2] & 1<<4) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_0,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_0,RESET);

if(tablica[2] & 1<<5) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,RESET);

if(tablica[2] & 1<<6) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,RESET);

if(tablica[2] & 1<<1) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,RESET);
//koniec druga grupa od lewej



//trzecia grupa od lewej (17-24)
if(tablica[3] & 1) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_4,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_4,RESET);

if(tablica[3] & 1<<1) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_5,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_5,RESET);

if(tablica[3] & 1<<2) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,RESET);

if(tablica[3] & 1<<3) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,RESET);

if(tablica[3] & 1<<4) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,RESET);

if(tablica[3] & 1<<5) HAL_GPIO_WritePin(GPIOC,GPIO_PIN_9,SET);
else HAL_GPIO_WritePin(GPIOC,GPIO_PIN_9,RESET);

if(tablica[3] & 1<<6) HAL_GPIO_WritePin(GPIOB,GPIO_PIN_10,SET);
else HAL_GPIO_WritePin(GPIOB,GPIO_PIN_10,RESET);

if(tablica[3] & 1<<1) HAL_GPIO_WritePin(GPIOB,GPIO_PIN_11,SET);
else HAL_GPIO_WritePin(GPIOB,GPIO_PIN_11,RESET);
//koniec czwarta grupa od lewej

//czwarta grupa od lewej (25-27)
if(tablica[4] & 1) HAL_GPIO_WritePin(GPIOB,GPIO_PIN_0,SET);
else HAL_GPIO_WritePin(GPIOB,GPIO_PIN_0,RESET);

if(tablica[4] & 1<<1) HAL_GPIO_WritePin(GPIOB,GPIO_PIN_1,SET);
else HAL_GPIO_WritePin(GPIOB,GPIO_PIN_1,RESET);

if(tablica[4] & 1<<2) HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,SET);
else HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,RESET);
//koniec czwarta grupa od lewej
*/


}

