/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "Sterowanie.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint16_t size=6;
uint8_t Received[6],Transmit[10];
uint8_t GPIO_PINSTATE[32];
uint8_t cnt[5];


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	if(Received[0]==0x71 && Received[5]==0x72)//JEZELI ODEBRANO RANKE Z DOBRYM POCZATKIEM I KONCEM
	{
	// HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_5);
	 HAL_GPIO_WritePin(LED_1_GPIO_Port,LED_1_Pin,SET);//gdy nie ma bledy mignij diod�
	 Transmit[0]=0x01;
	 HAL_UART_Transmit(&huart1,Transmit,1,10);//WYSLIJ ZE JEST OK
	 Received[0]=0xFF;
	 Received[5]=0xFF;
	 Transmit[0]=0xFF;

	 Sterowanie_przekaznikami(Received,size);
	 HAL_GPIO_WritePin(LED_1_GPIO_Port,LED_1_Pin,RESET);
	 HAL_GPIO_WritePin(LED_2_GPIO_Port,LED_2_Pin,RESET);//gdy blad zaswiec diode i zostaw ja
	}
	else
	{
	 HAL_GPIO_WritePin(LED_2_GPIO_Port,LED_2_Pin,RESET);//gdy blad zaswiec diode i zostaw ja
	Transmit[1]++;
	 //Transmit[0]=0x00;
	 //HAL_UART_Transmit(&huart1,Transmit,1,10);//WYSLIJ ZE COS POSZLO NIE TAK(GDZY TU WEJDZIE TRZEBA 2 RAZY WYSLAC TAKA SAMA RAMK� ZEBY WR�CI�O NA 100% DO NORMY
	 HAL_GPIO_WritePin(LED_2_GPIO_Port,LED_2_Pin,SET);

	}

 HAL_UART_Receive_IT(&huart1, Received, size); // Ponowne w��czenie nas�uchiwania
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();

  /* USER CODE BEGIN 2 */
  HAL_UART_Receive_IT(&huart1, Received, size);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

		 GPIO_PINSTATE[0]= HAL_GPIO_ReadPin(REL_01_GPIO_Port,REL_01_Pin);
		 GPIO_PINSTATE[1]= HAL_GPIO_ReadPin(REL_02_GPIO_Port,REL_02_Pin);
		 GPIO_PINSTATE[2]= HAL_GPIO_ReadPin(REL_03_GPIO_Port,REL_03_Pin);
		 GPIO_PINSTATE[3]= HAL_GPIO_ReadPin(REL_04_GPIO_Port,REL_04_Pin);
		 GPIO_PINSTATE[4]= HAL_GPIO_ReadPin(REL_05_GPIO_Port,REL_05_Pin);
		 GPIO_PINSTATE[5]= HAL_GPIO_ReadPin(REL_06_GPIO_Port,REL_06_Pin);
		 GPIO_PINSTATE[6]= HAL_GPIO_ReadPin(REL_07_GPIO_Port,REL_07_Pin);
		 GPIO_PINSTATE[7]= HAL_GPIO_ReadPin(REL_08_GPIO_Port,REL_08_Pin);

		 GPIO_PINSTATE[8]= HAL_GPIO_ReadPin(REL_09_GPIO_Port,REL_09_Pin);
		 GPIO_PINSTATE[9]= HAL_GPIO_ReadPin(REL_10_GPIO_Port,REL_10_Pin);
		 GPIO_PINSTATE[10]= HAL_GPIO_ReadPin(REL_11_GPIO_Port,REL_11_Pin);
		 GPIO_PINSTATE[11]= HAL_GPIO_ReadPin(REL_12_GPIO_Port,REL_12_Pin);
		 GPIO_PINSTATE[12]= HAL_GPIO_ReadPin(REL_13_GPIO_Port,REL_13_Pin);
		 GPIO_PINSTATE[13]= HAL_GPIO_ReadPin(REL_14_GPIO_Port,REL_14_Pin);
		 GPIO_PINSTATE[14]= HAL_GPIO_ReadPin(REL_15_GPIO_Port,REL_15_Pin);
		 GPIO_PINSTATE[15]= HAL_GPIO_ReadPin(REL_16_GPIO_Port,REL_16_Pin);

		 GPIO_PINSTATE[16]= HAL_GPIO_ReadPin(REL_17_GPIO_Port,REL_17_Pin);
		 GPIO_PINSTATE[17]= HAL_GPIO_ReadPin(REL_18_GPIO_Port,REL_18_Pin);
		 GPIO_PINSTATE[18]= HAL_GPIO_ReadPin(REL_19_GPIO_Port,REL_19_Pin);
		 GPIO_PINSTATE[19]= HAL_GPIO_ReadPin(REL_20_GPIO_Port,REL_20_Pin);
		 GPIO_PINSTATE[20]= HAL_GPIO_ReadPin(REL_21_GPIO_Port,REL_21_Pin);
		 GPIO_PINSTATE[21]= HAL_GPIO_ReadPin(REL_22_GPIO_Port,REL_22_Pin);
		 GPIO_PINSTATE[22]= HAL_GPIO_ReadPin(REL_23_GPIO_Port,REL_23_Pin);
		 GPIO_PINSTATE[23]= HAL_GPIO_ReadPin(REL_24_GPIO_Port,REL_24_Pin);

		 GPIO_PINSTATE[24]= HAL_GPIO_ReadPin(REL_25_GPIO_Port,REL_25_Pin);
		 GPIO_PINSTATE[25]= HAL_GPIO_ReadPin(REL_26_GPIO_Port,REL_26_Pin);
		 GPIO_PINSTATE[26]= HAL_GPIO_ReadPin(REL_27_GPIO_Port,REL_27_Pin);
		 HAL_Delay(100);
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
